import { applyMiddleware, compose, createStore } from 'redux'
import { routerMiddleware } from 'react-router-redux'
import thunk from 'redux-thunk'
import rootReducer from './rootReducer'
import landingTemplateApp from '../redux/reducers/all'

export default function configureStore(reducers, initialState = window.__INITIAL_STATE__) {

  // Create final store and subscribe router in debug env ie. for devtools
  const store = createStore(landingTemplateApp, initialState)

  return store
}
