export const visibilityFilter = (state = 'SHOW_ALL', action) => {
  switch (action.type) {
    case 'SET_VISIBILITY_FILTER':
      return action.filter
    default:
      return state
  }
}

// const mapStateToProps = (state) => {
//   return {
//     tasks: getVisibleTasks(state.tasks, state.visibilityFilter)
//   }
// }
//
// const mapDispatchToProps = (dispatch) => {
//   return {
//     onTaskClick: (id) => {
//       dispatch(toggleTask(id))
//     }
//   }
// }

export default visibilityFilter
