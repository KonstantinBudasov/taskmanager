import { combineReducers } from 'redux'

const initialState = {
  cards: {}
}

export const card = (state = initialState, action) => {
  switch (action.type) {
    case 'EXPAND_CARD':
      return [
        state,
        {
          isChecked: true
        }
      ]

    default:
      return state
  }
}

export const visibilityFilter = (state = 'SHOW_ALL', action) => {
  switch (action.type) {
    case 'SET_VISIBILITY_FILTER':
      return action.filter
    default:
      return state
  }
}

const landingTemplateApp = combineReducers({
  card,
  visibilityFilter
})

console.log(landingTemplateApp);

export default landingTemplateApp
