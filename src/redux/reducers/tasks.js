const task = (state, action) => {
  switch (action.type) {
    case 'ADD_TASK':
      return [
        ...state,
        {
          text: action.text,
          completed: false
        }
      ]

    case 'COMPLETE_TASK':
      var newState = Object.assign({}, state);

      newState.tasks.items[action.index].completed = true;

      return {
        ...newState
      }

    case 'TOGGLE_TASK':

      //newState.tasks.items[action.index].completed = true;

      return {
        ...newState
      }

    case 'DELETE_TASK':
      var items = [].concat(state.tasks.items);

      items.splice(action.index, 1);

      return Object.assign({}, state, {
        tasks: {
          items
        }
      })

    case 'CLEAR_TASK': // Using to clear Tasks list
      return Object.assign({}, state, {
        tasks: {
          items: []
        }
      })

    default:
      return state
  }
}

const tasks = (state = [], action) => {
  switch (action.type) {
    case 'ADD_TASK':
      return [
        ...state,
        task(undefined, action)
      ]
    case 'TOGGLE_TASK':
      return state.map(t =>
          task(t, action)
      )
    default:
      return state
  }
}

export default tasks
