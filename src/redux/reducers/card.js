const card = (state = initialState, action) => {
  switch (action.type) {
    case 'EXPAND_CARD':
      return [
        state,
        {
          isChecked: true
        }
      ]

    default:
      return state
}

const initialState = {
  visibilityFilter: VisibilityFilters.SHOW_ALL,
  cards: {}
}


export default card
