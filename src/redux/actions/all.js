let nextTaskId = 0

export const card = (title, description) => {
  return {
    type: 'DEFAULT_VIEW',
    id: nextTaskId++,
    completed: false,
    title,
    description
  }
}
export const addTask = (title, description) => {
  return {
    type: 'ADD_TASK',
    id: nextTaskId++,
    completed: false,
    title,
    description
  }
}

function completeTask(id) {
  return {
    type: 'COMPLETE_TASK',
    id
  };
}

function toggleTask(id) {
  return {
    type: 'TOGGLE_TASK',
    id,
    description
  };
}

function deleteTask(id) {
  return {
    type: 'DELETE_TASK',
    id
  };
}

function clearTask() {
  return {
    type: 'CLEAR_TASK'
  };
}
