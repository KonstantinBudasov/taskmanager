import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import HomeView from '../containers/HomeView'
import classes from './Task.scss'

const Card = ({onClick, title, description, completed}) => (

  <li className={classes['tasks-list-item']}>

    <div className={classes['task']}>
      <h2 onClick={onClick}
          className={classes['task-title']}>

          {title}

      </h2>

      <div className={classes['task-details']} isChecked={false}>
        <p className={classes['task-description']}>{description}</p>
      </div>

    </div>

  </li>
);

export default Card
