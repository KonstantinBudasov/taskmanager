import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import HomeView from '../containers/HomeView'
import { addTask } from '../redux/actions/all'
import classes from './AddTaskForm.scss'

let AddTask = ({ dispatch }) => {
  let inputTitleValue
  let inputDescriptionValue

  return (
    <form action="#"
          onSubmit={e => {
              e.preventDefault()
              if (!inputTitleValue.value.trim()) {
                  return
              }
              if (!inputDescriptionValue.value.trim()) {
                  return
              }
              dispatch(addTask(inputTitleValue.value, inputDescriptionValue.value))
              inputTitleValue.value = ''
              inputDescriptionValue.value = ''
          }}
          className={classes['form-add-task']}>
      <div className={classes['form-row']}>
        <div className={classes['form-item']}>
          <input type="text"
                 ref={node => {
                    inputTitleValue = node
                 }}
                 className={classes['form-control']}
                 placeholder="Title"/>
        </div>
        <div className={classes['form-item']}>
          <input type="text"
                 ref={node => {
                    inputDescriptionValue = node
                 }}
                 className={classes['form-control']}
                 placeholder="Description" />
        </div>
        <div className={classes['form-item']}>
          <input type="submit"
                 value="Add Entry"
                 className={classes['form-submit']}/>
        </div>
      </div>
    </form>
  )
}
AddTask = connect()(AddTask)

export default AddTask
