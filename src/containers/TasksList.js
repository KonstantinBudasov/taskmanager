import React, {PropTypes} from 'react'
import { applyMiddleware, compose, createStore } from 'redux'
import { connect } from 'react-redux'
import HomeView from '../containers/HomeView'
import Task from '../components/Task'
import classes from './TasksList.scss'
import configureStore from '../redux/configureStore'
import { addTask, toggleTask } from '../redux/actions/all'
import taskManagerApp from '../redux/reducers/all'

const TasksList = ({tasks, onTaskClick}) => (

  <ul className={classes['tasks-list']}>
    {tasks.map(task =>
      <Task title={task.title}
            description={task.description}
            isCompleted={task.completed}
            key={task.id}
            {...task}
            onClick={onTaskClick}
      />
    )}
  </ul>
);

TasksList.propTypes = {
  tasks: PropTypes.arrayOf(PropTypes.shape({
    //isCompleted: PropTypes.bool.isRequired,
    //description: PropTypes.string.isRequired
  }).isRequired).isRequired,
  onTaskClick: PropTypes.func.isRequired
}

const mapDispatchToProps = (dispatch) => {
  return {
    onTaskClick: (id) => {
      dispatch(toggleTask(id))
    }
  }
}

export default TasksList
