import React, {PropTypes} from 'react'
import { applyMiddleware, compose, createStore } from 'redux'
import { connect } from 'react-redux'
//import configureStore from '../redux/configureStore'
import HomeView from '../containers/HomeView'
import classes from './CardsList.scss'
import { addTask, toggleTask } from '../redux/actions/all'
import landingTemplateApp from '../redux/reducers/all'

let cards = window.__INITIAL_STATE__.cards;

const CardsList = ({cards, onCardClick}) => (

  <ul className={classes['cards-list']} cards={cards}>
    {cards.map(card =>
      <Card title={card.title}
            description={card.description}
            isChecked={card.checked}
            key={card.id}
            {...card}
            onClick={onCardClick}
      />
    )}
  </ul>
);
//
// CardsList.propTypes = {
//   tasks: PropTypes.arrayOf(PropTypes.shape({
//     //isCompleted: PropTypes.bool.isRequired,
//     //description: PropTypes.string.isRequired
//   }).isRequired).isRequired,
//   onTaskClick: PropTypes.func.isRequired
// }

// const mapDispatchToProps = (dispatch) => {
//   return {
//     onTaskClick: (id) => {
//       //dispatch(toggleTask(id))
//     }
//   }
// }

export default CardsList
