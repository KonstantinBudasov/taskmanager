import React, { PropTypes } from 'react'
import { Provider } from 'react-redux'
import { hashHistory } from 'react-router'
import { Router } from 'react-router'
import { createStore } from 'redux'

export default class Root extends React.Component {
  static propTypes = {
    routes: PropTypes.element.isRequired,
    store: PropTypes.object.isRequired
  };

  get content () {
    return (
      <Router history={hashHistory}>
        {this.props.routes}
      </Router>
    )
  }

  render () {
    return (
      <Provider store={this.props.store}>
        <div style={{ height: '100%' }}>
          {this.content}
        </div>
      </Provider>
    )
  }
}
