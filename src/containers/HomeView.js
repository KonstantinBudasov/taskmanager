import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import classes from './HomeView.scss'
//import { applyMiddleware, compose, createStore } from 'redux'
import TasksList from './TasksList'
import Task from '../components/Task'
//import configureStore from '../redux/configureStore'
import landingTemplateApp from '../redux/reducers/all'
import CardsList from '../containers/CardsList'
//import { addTask, toggleTask } from '../redux/actions/all'

//let store = configureStore(landingTemplateApp, window.__INITIAL_STATE__)
let cards = window.__INITIAL_STATE__.cards;

//console.log(store);

export class HomeView extends React.Component<void, Props, void> {

  render () {
    return (
      <div className='container-fluid'>
        <div className='row-fluid'>
          <div className='col-md-8'>
            <h1 className={classes['main-title']}>Веб-лаборатория</h1>

            <h2 className='subtitle'>Promote your product or service in compact subtitle</h2>

            <CardsList cards={cards} />

          </div>

          <div className='col-md-4'>
          </div>
        </div>
      </div>
    )
  }
}

// Log the initial state
//console.log(store.getState())

// Every time the state changes, log it
// Note that subscribe() returns a function for unregistering the listener
// let unsubscribe = store.subscribe(() =>
//   console.log(store.getState())
// )

// Dispatch some actions
//store.dispatch(addTask('Learn about actions', 'Really useful description'))
//console.log(store.dispatch(addTask('Learn about reducers', 'Really useful description')))
//console.log(store.dispatch(addTask('Learn about store', 'Really useful description')))
//configureStore().dispatch(toggleTask(0))

// Stop listening to state updates
//unsubscribe()

export default HomeView
